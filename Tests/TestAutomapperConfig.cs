using ReservationEngine.Mappings;
using System;
using Application.Contracts;
using Xunit;

namespace Tests
{
    public class TestAutomapperConfig
    {
        [Fact]
        public void TestMappingConfiguration()
        {
            var mapper = Mappings.RegisterMappings();

            var mock = new Moq.Mock<ReservationEngine.Contracts.ArrangementDto>();

            var mappedObject = mapper.Map<ArrangementDto>(mock.Object);

            Assert.NotNull(mappedObject);

        }
    }
}
