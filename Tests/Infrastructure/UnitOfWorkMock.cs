﻿using Infrastructure.Context;
using Infrastructure.Interfaces;
using Infrastructure.Poco;
using Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests.Infrastructure
{
    public class UnitOfWorkMock : IUnitOfWork
    {
        private readonly IDatabaseContext _context;

        public IGenericRepository<KeyTable> KeyTableRepository { get; set; }
        public IGenericRepository<ResArrangement> ResArrangementRepository { get; set; }
        public IGenericRepository<ResPart> ResPartRepository { get; set; }
        public IGenericRepository<Reservation> ReservationRepository { get; set; }
        public IGenericRepository<ResEvent> ResEventRepository { get; set; }
        public IGenericRepository<ResArrPreferedSchedule> ResArrPreferedScheduleRepository { get; set; }

        public UnitOfWorkMock()
        {
            var options = new DbContextOptionsBuilder<DatabaseContext>()
               .UseInMemoryDatabase(databaseName: "TestDb")
               .Options;

            _context = new DatabaseContext(options);

            _context.GetDbSet<Reservation>().Add(new Reservation { ReservationId = 1, VisitDate = DateTime.Now.AddDays(2), StartDateTime = DateTime.Now, EndDateTime = DateTime.Now.AddDays(1), GrossAmount = 124.35m, NetAmount = 124.20m, Transportation = "i", });

            _context.GetDbSet<ResPart>().Add(new ResPart { ReservationId = 1, ResPartId = 1, LineNumber = 100000, PartType = "R", PartId = 627, PartDescription = "Raveleijn Diner excl. show, excl. entree", ResArrangementId = 3, Starts = DateTime.Now.AddDays(-2), Ends = DateTime.Now.AddDays(30), Quantity = 6, CostPrice = 0, Price = 25, PriceExcl = 20, GrossAmount = 180, GrossAmountExcl = 150, DiscountPerc = 0, NetAmount = 180, NetAmountExcl = 180, });
            _context.GetDbSet<ResPart>().Add(new ResPart { ReservationId = 1, ResPartId = 2, LineNumber = 300000, PartType = "L", DescriptionMemo = "Restaurant Het Wapen van Raveleijn is gelegen in het Marerijk, de ingang bevindt zich tussen de parkshow Raveleijn en Villa Volta.", PartId = 52, PartDescription = "Wapen van Raveleijn", ResLocationId = 1, ResArrangementId = 3, Starts = DateTime.Now.AddDays(-2), Ends = DateTime.Now.AddDays(30), Quantity = 6, CostPrice = 0, Price = 0, PriceExcl = 0, GrossAmount = 0, GrossAmountExcl = 0, DiscountPerc = 0, NetAmount = 0, NetAmountExcl = 0, ResArrPartId = 2150, FixedQuantity = "N", ResArrPartQuantity = 1 });
            _context.GetDbSet<ResPart>().Add(new ResPart { ReservationId = 1, ResPartId = 3, LineNumber = 400000, PartType = "A", PartId = 627, PartDescription = "Raveleijn diner volwassen voucher", ResArrangementId = 3, Starts = DateTime.Now.AddDays(-2), Ends = DateTime.Now.AddDays(30), Quantity = 6, CostPrice = 0, Price = 25, PriceExcl = 20, GrossAmount = 180, GrossAmountExcl = 150, DiscountPerc = 0, NetAmount = 180, NetAmountExcl = 180, ResArrPartId = 2150, FixedQuantity = "N", ResArrPartQuantity = 1 });
            _context.GetDbSet<ResPart>().Add(new ResPart { ReservationId = 1, ResPartId = 4, LineNumber = 500000, PartType = "A", PartId = 627, PartDescription = "Raveleijn diner volwassen voucher", ResArrangementId = 3, Starts = DateTime.Now.AddDays(-2), Ends = DateTime.Now.AddDays(30), Quantity = 6, CostPrice = 0, Price = 25, PriceExcl = 20, GrossAmount = 180, GrossAmountExcl = 150, DiscountPerc = 0, NetAmount = 180, NetAmountExcl = 180, ResArrPartId = 2150, FixedQuantity = "N", ResArrPartQuantity = 1 });

            _context.GetDbSet<ResArrangement>().Add(new ResArrangement { ResArrangementId = 3, ArticleId = 2, Description = "Raveleijn Diner excl. show, excl. entree", TicketText = "Raveleijn diner excl. show", OrderNumber = 627, UseAsTemplate = false, FromDate = DateTime.Now, ToDate = DateTime.Now.AddDays(30), IsScheduledOnInternet = false, IsScheduledOnBrp = false });

            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 1, ResArrangementId = 3, DateFrom = DateTime.Now.AddDays(-4), Blocked = false });
            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 2, ResArrangementId = 3, DateFrom = DateTime.Now.AddDays(-3), Blocked = false });
            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 3, ResArrangementId = 3, DateFrom = DateTime.Now.AddDays(-2), Blocked = false });
            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 4, ResArrangementId = 3, DateFrom = DateTime.Now.AddDays(-1), Blocked = false });
            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 5, ResArrangementId = 3, DateFrom = DateTime.Now, Blocked = false });
            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 6, ResArrangementId = 3, DateFrom = DateTime.Now.AddDays(1), Blocked = false });
            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 7, ResArrangementId = 3, DateFrom = DateTime.Now.AddDays(2), Blocked = false });
            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 8, ResArrangementId = 3, DateFrom = DateTime.Now.AddDays(3), Blocked = false });
            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 9, ResArrangementId = 3, DateFrom = DateTime.Now.AddDays(4), Blocked = false });
            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 10, ResArrangementId = 3, DateFrom = DateTime.Now.AddDays(5), Blocked = false });
            _context.GetDbSet<ResArrPreferedSchedule>().Add(new ResArrPreferedSchedule { ResArrPreferedScheduleId = 11, ResArrangementId = 3, DateFrom = DateTime.Now.AddDays(6), Blocked = false });

            KeyTableRepository = new GenericRepository<KeyTable>(_context);
            ResArrangementRepository = new GenericRepository<ResArrangement>(_context);
            ResPartRepository = new GenericRepository<ResPart>(_context);
            ReservationRepository = new GenericRepository<Reservation>(_context);
            ResEventRepository = new GenericRepository<ResEvent>(_context);
            ResArrPreferedScheduleRepository = new GenericRepository<ResArrPreferedSchedule>(_context);

            _context.SaveChanges();

        }

        [Fact]
        public void UnitOfWorkMockTest()
        {
            var count = _context.GetDbSet<ResPart>().ToListAsync<ResPart>();
            Assert.Equal(4, count.Result.Count);

            var countResArrange = _context.GetDbSet<ResArrangement>().ToListAsync<ResArrangement>();
            Assert.Single(countResArrange.Result);

            var reservations = _context.GetDbSet<Reservation>().ToListAsync<Reservation>();
            Assert.Single(reservations.Result);
        }

        public void Save()
        {
            
        }
    }
}

