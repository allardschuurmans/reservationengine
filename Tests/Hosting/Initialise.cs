﻿using Application.Interfaces;
using Application.Services;
using Autofac;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Microsoft.Extensions.Logging;
using ReservationEngine.Mappings;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Tests.Infrastructure;

namespace Tests.Hosting
{
    public class Initialise
    {
        private readonly IContainer _container;

        public Initialise()
        {
            var builder = new ContainerBuilder();

            builder.RegisterInstance(new LoggerFactory().AddSerilog()).As<ILoggerFactory>();

            builder.RegisterGeneric(typeof(Logger<>)).As(typeof(ILogger<>));
            builder.RegisterType<DatabaseContext>().As<IDatabaseContext>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWorkMock>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<ArrangementService>().As<IArrangementService>().InstancePerLifetimeScope();
            
            builder.Register(contx =>
            {
                var cts = contx.Resolve<IComponentContext>();
                var config = cts.Resolve<MapperConfiguration>();

                return config.CreateMapper();

            }).As<IMapper>();


            
            _container = builder.Build();

        }

        public IContainer GetContainer()
        {
            return _container;
        }
    }
}
