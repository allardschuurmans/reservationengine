﻿using Application.Interfaces;
using Microsoft.AspNetCore;
using ReservationEngine;
using System;
using Autofac.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Context;
using Application.Services;
using Autofac;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Infrastructure.Interfaces;
using Serilog;
using Tests.Infrastructure;

namespace Tests.Application
{
    public class ServiceTests
    {
        private readonly IArrangementService _arrangementService;

        public ServiceTests()
        {
            var initialise = new Hosting.Initialise();

            using (var scope = initialise.GetContainer().BeginLifetimeScope())
            {
                var logger = scope.Resolve<ILogger<ArrangementService>>();
                var unitOfWork = scope.Resolve<IUnitOfWork>();
                var mapper = scope.Resolve<IMapper>();

                _arrangementService = new ArrangementService(logger, unitOfWork, mapper);
            }
        }

        [Fact]
        public void TestArrangementService()
        {
            var arrange = _arrangementService.GetArrangementById(1);

        }
    }
}
