﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationEngine.Contracts
{
    public class ResArrPreferedScheduleDto
    {
        public long ResArrPreferedScheduleId { get; set; }
        public long? ResArrangementId { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? Starttime { get; set; }
        public DateTime? Endtime { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public bool? Blocked { get; set; }
        public bool? AvailableOnInternet { get; set; }
        public bool? AvailableOnBrp { get; set; }
        public bool? AvailableOnCashregister { get; set; }
        public long? ResArrPreferedScheduleSourceId { get; set; }
    }
}
