﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationEngine.Contracts
{
    public class ReservationEventDto
    {
        public long ResEventId { get; set; }
        public long? RelationId { get; set; }
        public string PartyName { get; set; }
        public string PartyKind { get; set; }
        public long? ResGroupingId { get; set; }
        public long? ResStatusId { get; set; }
        public long? ResCancelCauseId { get; set; }
        public long? ResSourceId { get; set; }
        public long? RepresentativeId { get; set; }
        public long? PaymentId { get; set; }
        public decimal? GrossAmount { get; set; }
        public decimal? DiscountPerc { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? DownPaymentPerc { get; set; }
        public int? TotalQuantity { get; set; }
        public int? QuantityAgeCategory1 { get; set; }
        public int? QuantityAgeCategory2 { get; set; }
        public int? QuantityAgeCategory3 { get; set; }
        public int? QuantityAgeCategory4 { get; set; }
        public bool? MakeOffer { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public DateTime? CreationDate { get; set; }
        public long? CreationUser { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public DateTime? ValidUntil { get; set; }
        public bool? VideoPayed { get; set; }
        public decimal? GrossAmountExcl { get; set; }
        public decimal? NetAmountExcl { get; set; }
        public long? CorrespondenceRelationId { get; set; }
        public long? ResOriginId { get; set; }
        public long? ResGroupId { get; set; }
        public decimal? PayedAmount { get; set; }
        public string FolioNr { get; set; }
        public int? State { get; set; }
        public int? HistQuantityAgeCategory1 { get; set; }
        public int? HistQuantityAgeCategory2 { get; set; }
        public int? HistQuantityAgeCategory3 { get; set; }
        public int? HistQuantityAgeCategory4 { get; set; }
        public int? HistTotalQuantity { get; set; }
        public int? InternalPaymentState { get; set; }
        public int? OnlinePaymentProvider { get; set; }
        public string ExternalReference { get; set; }
        public string PaymentReferenceNumber { get; set; }
        public long? RelationContactId { get; set; }
        public bool? Processed { get; set; }
        public long? ResSubGroupingId { get; set; }
        public long? InvoiceRelationId { get; set; }
        public string Guid { get; set; }
    }
}
