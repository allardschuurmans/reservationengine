﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservationEngine.Contracts
{
    public class ReservationDto
    {
        public long ReservationId { get; set; }
        public long? ResEventId { get; set; }
        public DateTime? VisitDate { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public decimal? GrossAmount { get; set; }
        public decimal? DiscountPerc { get; set; }
        public decimal? NetAmount { get; set; }
        public string Transportation { get; set; }
        public string Remarks { get; set; }
        public string MemoInternal { get; set; }
        public string MemoExternal { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public bool? Processed { get; set; }
        public decimal? GrossAmountExcl { get; set; }
        public decimal? NetAmountExcl { get; set; }
        public bool? ActionUsed { get; set; }
    }
}
