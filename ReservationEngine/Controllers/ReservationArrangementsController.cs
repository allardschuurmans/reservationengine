﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ReservationEngine.Controllers
{
    [Produces("application/json")]
    [Route("api/Reservations/{reservationId}/Arrangements")]
    public class ReservationArrangementsController : Controller
    {
        private readonly ILogger<ReservationArrangementsController> _logger;
        private readonly IArrangementService _arrangementService;

        public ReservationArrangementsController(ILogger<ReservationArrangementsController> logger, IArrangementService arrangementService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _arrangementService = arrangementService ?? throw new ArgumentNullException(nameof(arrangementService));
        }
    }
}
