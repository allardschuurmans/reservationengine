﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ReservationEngine.Controllers
{
    [Produces("application/json")]
    [Route("api/Relation/{relationId}/Reservations")]
    public class ReservationsController : Controller
    {
        private readonly ILogger<ReservationsController> _logger;

        public ReservationsController(ILogger<ReservationsController> logger) => _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        
        // GET: api/Reservations/5
        [HttpGet]
        public string Get(int relationId)
        {
            return "value";
        }

    }
}
