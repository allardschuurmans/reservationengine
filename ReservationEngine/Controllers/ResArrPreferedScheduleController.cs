﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Application.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReservationEngine.Contracts;

namespace ReservationEngine.Controllers
{
    [Produces("application/json")]
    [Route("api/Arrangements/{arrangementId}/ResArrPreferedSchedule/")]
    public class ResArrPreferedScheduleController : Controller
    {
        private readonly ILogger<ResArrPreferedScheduleController> _logger;
        private readonly IArrangementService _arrangementService;

        public ResArrPreferedScheduleController(ILogger<ResArrPreferedScheduleController> logger, IArrangementService arrangementService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _arrangementService = arrangementService ?? throw new ArgumentNullException(nameof(arrangementService));
        }

        [HttpGet()]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(List<ResArrPreferedScheduleDto>), 200)]
        [ResponseCache(Duration = 3600)]
        public async Task<IActionResult> GetReservationEvent(long arrangementId)
        {
            var preferedScheduleDtos = await _arrangementService.GetResArrPreferedSchedule(arrangementId);

            return new ObjectResult(preferedScheduleDtos);
        }

        [HttpGet("GetReservationEventByDate")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(List<ResArrPreferedScheduleDto>), 200)]
        public async Task<IActionResult> GetReservationEventByDate(long arrangementId, DateTime from, DateTime to)
        {
            var preferedScheduleDtos = await _arrangementService.GetResArrPreferedScheduleDate(arrangementId, from, to);

            return new ObjectResult(preferedScheduleDtos);
        }

    }
}
