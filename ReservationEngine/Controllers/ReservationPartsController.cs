﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReservationEngine.Contracts;

namespace ReservationEngine.Controllers
{
    [Produces("application/json")]
    [Route("api/Reservations/{reservationId}/ReservationParts")]
    public class ReservationPartsController : Controller
    {
        private readonly ILogger<ReservationPartsController> _logger;
        private readonly IReservationService _reservationService;
        private readonly IMapper _mapper;

        public ReservationPartsController(ILogger<ReservationPartsController> logger,
            IReservationService reservationService, IMapper mapper)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _reservationService = reservationService ?? throw new ArgumentNullException(nameof(reservationService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        // GET: api/Reservations/5
        [HttpGet()]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(List<ReservationPartDto>), 200)]
        public async Task<IActionResult> GetReservationPartsByResId(long reservationId, string partType, DateTime from, DateTime to)
        {
            var arrangement = await _reservationService.GetReservationPartById(reservationId, partType, from, to);

            if (arrangement != null)
                return new ObjectResult(arrangement);
            else
                return NotFound();
        }
    }
}
