﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReservationEngine.Contracts;

namespace ReservationEngine.Controllers
{
    [Produces("application/json")]
    [Route("api/Arrangements/{arrangementId}/ArrangementPart")]
    public class ArrangementPartController : Controller
    {
        private readonly ILogger<ArrangementPartController> _logger;
        private readonly IArrangementService _arrangementService;
        private readonly IMapper _mapper;

        public ArrangementPartController(ILogger<ArrangementPartController> logger, IArrangementService arrangementService, IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _arrangementService = arrangementService ?? throw new ArgumentNullException(nameof(arrangementService));
        }

        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(List<ArrangementPartDto>), 200)]
        public async Task<IActionResult> GetParts(long arrangementId, string partType)
        {
            if (arrangementId > long.MinValue)
            {
                var value = await _arrangementService.GetArrangementPartsById(arrangementId, partType);

                if (value != null)
                {
                    var arrangement = _mapper.Map<List<ArrangementPartDto>>(value);
                    return new ObjectResult(arrangement);
                }
                else
                {
                    return NotFound();
                }
            }

            return BadRequest();
        }

        // POST: api/ResArrangementPart
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/ResArrangementPart/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
