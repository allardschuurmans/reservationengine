﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ReservationEngine.Controllers
{
    [Produces("application/json")]
    [Route("api/Reservations/{reservationId}/ReservationsEvent")]
    public class ReservationEventController : Controller
    {
        private readonly ILogger<ReservationEventController> _logger;
        private readonly IReservationService _reservationService;
        private readonly IMapper _mapper;

        public ReservationEventController(ILogger<ReservationEventController> logger, IReservationService reservationService, IMapper mapper)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _reservationService = reservationService ?? throw new ArgumentNullException(nameof(reservationService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        // GET: api/Reservations/
        [HttpGet()]
        public async Task<IEnumerable<Contracts.ReservationEventDto>> GetReservationEvents(long reservationId)
        {
            var reservationEvents = await _reservationService.GetReservationEventsByReservationId(reservationId);

            return _mapper.Map<IEnumerable<Contracts.ReservationEventDto>>(reservationEvents);
        }

        // GET: api/Reservations/5
        [HttpGet("{reservationEventId}")]
        public async Task<Contracts.ReservationEventDto> GetReservationEvent(long reservationId, long reservationEventId)
        {
            var reservationEventDto = await _reservationService.GetReservationEventById((long)reservationEventId);

            return _mapper.Map<Contracts.ReservationEventDto>(reservationEventDto);
        }

       
    }
}
