﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Application.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReservationEngine.Contracts;

namespace ReservationEngine.Controllers
{
    [Produces("application/json")]
    [Route("api/ReservationLocations")]
    public class ResLocationController : Controller
    {
        private readonly ILogger<ResLocationController> _logger;
        private readonly IReservationService _reservationService;
        private readonly IMapper _mapper;

        public ResLocationController(ILogger<ResLocationController> logger, IReservationService reservationService, IMapper mapper)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _reservationService = reservationService ?? throw new ArgumentNullException(nameof(reservationService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet()]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(List<ResLocationOccupationDto>), 200)]
        [ResponseCache(Duration = 60)]
        public async Task<IActionResult> Get(long locationId, DateTime startDateTime, DateTime endDateTime)
        {
            var location = await _reservationService.GetResLocationOccupationByLocationId(locationId, startDateTime, endDateTime);

            var result = _mapper.Map<List<ResLocationOccupationDto>>(location);

            return new ObjectResult(result);
        }

    }
}
