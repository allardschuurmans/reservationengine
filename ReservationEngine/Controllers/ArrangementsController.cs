﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Application.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReservationEngine.Contracts;

namespace ReservationEngine.Controllers
{
    [Produces("application/json")]
    [Route("api/Arrangements")]
    public class ArrangementsController : Controller
    {
        private readonly ILogger<ArrangementsController> _logger;
        private readonly IArrangementService _arrangementService;

        public ArrangementsController(ILogger<ArrangementsController> logger, IArrangementService arrangementService)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _arrangementService = arrangementService ?? throw new ArgumentNullException(nameof(arrangementService));
        }

        // GET: api/Reservations/
        [HttpGet()]
        public async Task<IActionResult> GetArrangements()
        {
            throw new NotImplementedException();
        }

        // GET: api/Reservations/5
        [HttpGet("{arrangementId}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ArrangementDto), 200)]
        public async Task<IActionResult> GetArrangementById(long arrangementId)
        {
            var arrangement = await _arrangementService.GetArrangementById(arrangementId);

            if (arrangement != null)
                return new ObjectResult(arrangement);
            else
                return NotFound();
        }

        // POST: api/Reservations
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/Arrangements/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
