﻿using Application.Contracts;
using AutoMapper;
using ReservationEngine.Interfaces;

namespace ReservationEngine.Mappings.Application
{
    public class ApplicationProfile : Profile
    {
        public ApplicationProfile()
        {
            CreateMap<ArrangementDto, Contracts.ArrangementDto>();
            CreateMap<ArrangementPartDto, Contracts.ArrangementPartDto>();
            CreateMap<ReservationDto, Contracts.ReservationDto>();
            CreateMap<ReservationPartDto, Contracts.ReservationPartDto>();
            CreateMap<ReservationEventDto, Contracts.ReservationEventDto>();
            CreateMap<ResArrPreferedScheduleDto, Contracts.ResArrPreferedScheduleDto>();
            CreateMap<ResLocationOccupationDto, Contracts.ResLocationOccupationDto>();
        }
    }
}
