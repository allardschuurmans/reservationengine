﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Contracts;
using AutoMapper;
using Infrastructure.Poco;
using ReservationEngine.Interfaces;

namespace ReservationEngine.Mappings.Infrastructure
{
    public class InfrastructureProfile : Profile
    {
        public InfrastructureProfile()
        {
            CreateMap<ResArrangement, ArrangementDto>();
            CreateMap<ResArrPart, ArrangementPartDto>();
            CreateMap<ResPart, ReservationPartDto>();
            CreateMap<ResLocationOccupation, ResLocationOccupationDto>();
        }
    }
}
