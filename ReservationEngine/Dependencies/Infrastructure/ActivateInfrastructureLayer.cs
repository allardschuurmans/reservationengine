using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Infrastructure.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ReservationEngine.Dependencies.InfrastructureLayer
{
    public static class ActivateInfrastructureLayer
    {
        public static IServiceCollection AddInfrastructureDependencies(IServiceCollection services, IConfiguration Configuration)
        {
            services.AddTransient<IUnitOfWork, ReservationUnitOfWork>();
            services.AddTransient<IDatabaseContext, DatabaseContext>();
            services.AddDbContext<Infrastructure.Context.DatabaseContext>(
                         options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            return services;
        }

    }

}