using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace ReservationEngine.Dependencies
{
    public static class StartupConfiguredServices
    {
        public static void AddDependencies(IServiceCollection services, IConfiguration configuration)
        {
            
            Hosting.ActivateHostingDependencies.AddHostingDependencies(services);
            Application.ActivateApplicationLayer.AddApplicationDependencies(services);
            Domain.ActivateDomainLayer.AddDomainDependencies(services);
            InfrastructureLayer.ActivateInfrastructureLayer.AddInfrastructureDependencies(services, configuration);

            //Add autofac
            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.Build();

        }
    }

}