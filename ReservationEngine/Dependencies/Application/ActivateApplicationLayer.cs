using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Interfaces;
using Application.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ReservationEngine.Dependencies.Application
{
    public static class ActivateApplicationLayer
    {
        public static IServiceCollection AddApplicationDependencies(IServiceCollection services)
        {
            services.AddTransient<IArrangementService, ArrangementService>();
            services.AddTransient<IReservationService, ReservationService>();
            return services;
        }

    }

}