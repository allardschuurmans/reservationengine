﻿using Application.Contracts;
using Application.Interfaces;
using Infrastructure.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Application.Services
{
    public class ReservationService : IReservationService
    {
        private readonly ILogger<ReservationService> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ReservationService(ILogger<ReservationService> logger, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        public Task<List<ArrangementDto>> GetArrangementsByReservationId(long reservationId)
        {
            throw new NotImplementedException();
        }

        public Task<ReservationDto> GetReservationById(long id)
        {
            var foundReservation = _unitOfWork.ReservationRepository.FindByAsync(m => m.ReservationId == id && m.ModifyKind != "D");

            return null;
        }

        public async Task<ReservationEventDto> GetReservationEventById(long id)
        {
            var reservationEvents = await _unitOfWork.ResEventRepository.GetEntityAsync(id);

            return _mapper.Map<ReservationEventDto>(reservationEvents);
        }

        public async Task<List<ReservationEventDto>> GetReservationEventsByReservationId(long reservationId)
        {
            var reservation = await _unitOfWork.ReservationRepository.GetEntityAsync(reservationId);
            var reservationEvents = await _unitOfWork.ResEventRepository.FindByAsync(m => m.ResEventId == reservation.ResEventId);

            return _mapper.Map<List<ReservationEventDto>>(reservationEvents);
        }

        public async Task<List<ReservationPartDto>> GetReservationPartById(long arrangementId, string partType, DateTime from, DateTime to)
        {
            var reservationParts = await _unitOfWork.ReservationPartRepository.FindByAsync(m => m.PartType == partType && m.ResArrangementId == arrangementId && m.ModifyKind != "D" && m.Starts > from && m.Ends > to);

            return _mapper.Map<List<ReservationPartDto>>(reservationParts);
        }

        public async Task<List<ReservationPartDto>> GetReservationPartsByReservationId(long reservationId)
        {
            var parts = await _unitOfWork.ReservationPartRepository.FindByAsync(m =>
                m.ReservationId == reservationId && m.ModifyKind != "D");

            return _mapper.Map<List<ReservationPartDto>>(parts);
        }

        public async Task<List<ResLocationOccupationDto>> GetResLocationOccupationByLocationId(long locationId, DateTime startDateTime, DateTime endDateTime)
        {
            var locationPoco =
                await _unitOfWork.ResLocationOccupationRepo.FindByAsync(m => m.ResLocationId == locationId && m.ModifyKind != "D" && m.Starts > startDateTime && m.Ends <= endDateTime);

            return _mapper.Map<List<ResLocationOccupationDto>>(locationPoco);
        }
    }
}
