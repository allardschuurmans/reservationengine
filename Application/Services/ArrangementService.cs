﻿using Application.Contracts;
using Application.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Infrastructure.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ArrangementService : IArrangementService
    {
        private readonly ILogger<ArrangementService> _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ArrangementService(ILogger<ArrangementService> logger, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<ArrangementPartDto> GetArrangementPartById(long arrangementPartId)
        {
            var part = await _unitOfWork.ResPartRepository.GetEntityAsync(arrangementPartId);

            var partDto = _mapper.Map<ArrangementPartDto>(part);

            return partDto;
        }

        public async Task<List<ArrangementPartDto>> GetArrangementPartsById(long arrangementId, string partType)
        {
            var arrangementPart = await _unitOfWork.ResPartRepository.FindByAsync(m => m.ResArrangementId == arrangementId && m.ModifyKind != "D" && m.PartType == partType);

            var list = _mapper.Map<List<ArrangementPartDto>>(arrangementPart);

            return list;
        }

        public async Task<ArrangementDto> GetArrangementById(long arrangementId)
        {
            var arrangement = await _unitOfWork.ResArrangementRepository.GetEntityAsync(arrangementId);

            var map = _mapper.Map<ArrangementDto>(arrangement);

            return map;
        }

        public async Task<List<ResArrPreferedScheduleDto>> GetResArrPreferedSchedule(long arrangementId)
        {
            var prefSchedule =
                await _unitOfWork.ResArrPreferedScheduleRepository.FindByAsync(m =>
                    m.ResArrangementId == arrangementId && m.ModifyKind != "D");

            var mappedPrefSchedule = _mapper.Map<List<ResArrPreferedScheduleDto>>(prefSchedule);

            return mappedPrefSchedule;
        }

        public Task<List<ArrangementDto>> GetArrangementsByReservationId(long reservationId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ResArrPreferedScheduleDto>> GetResArrPreferedScheduleDate(long arrangementId, DateTime from, DateTime to)
        {
            var prefSchedule =
                await _unitOfWork.ResArrPreferedScheduleRepository.FindByAsync(m =>
                    m.ResArrangementId == arrangementId && m.ModifyKind != "D" && m.DateFrom.Value > from.Date && m.DateFrom.Value <= to.Date);

            var mappedPrefSchedule = _mapper.Map<List<ResArrPreferedScheduleDto>>(prefSchedule);

            return mappedPrefSchedule;
        }
    }
}
