﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Contracts
{
   public  class ReservationPartDto
    {
        public long ResPartId { get; set; }
        public long? ReservationId { get; set; }
        public int? LineNumber { get; set; }
        public long? SetupResPartId { get; set; }
        public long? BreakDownResPartId { get; set; }
        public long? ParentResPartId { get; set; }
        public string PartType { get; set; }
        public long? PartId { get; set; }
        public string PartDescription { get; set; }
        public long? ResLocationId { get; set; }
        public long? DepartmentId { get; set; }
        public long? ResPlacingId { get; set; }
        public long? ResArrangementId { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        public int? Quantity { get; set; }
        public decimal? CostPrice { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceExcl { get; set; }
        public decimal? GrossAmount { get; set; }
        public decimal? GrossAmountExcl { get; set; }
        public double? DiscountPerc { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? NetAmountExcl { get; set; }
        public string Remarks { get; set; }
        public string DescriptionMemo { get; set; }
        public string TicketText1 { get; set; }
        public string TicketText2 { get; set; }
        public string TicketText3 { get; set; }
        public string TicketText4 { get; set; }
        public string TicketText5 { get; set; }
        public string TicketText6 { get; set; }
        public string TicketText7 { get; set; }
        public string TicketText8 { get; set; }
        public string TicketText9 { get; set; }
        public bool? SeparateTickets { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public string DescrMemoInt { get; set; }
        public bool? OnResQuote { get; set; }
        public long? ArrHeaderResPartId { get; set; }
        public bool? Exclusive { get; set; }
        public long? ResArrPartId { get; set; }
        public string FixedQuantity { get; set; }
        public int? ResArrPartQuantity { get; set; }
        public int? PayedQuantity { get; set; }
    }
}
