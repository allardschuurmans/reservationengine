﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Contracts
{
    public class ArrangementPartDto
    {
        public long ResArrPartId { get; set; }
        public long? ResArrangementId { get; set; }
        public int? LineNumber { get; set; }
        public int? AgeCategory { get; set; }
        public string PartType { get; set; }
        public long? PartId { get; set; }
        public string PartDescription { get; set; }
        public long? ResLocationId { get; set; }
        public long? DepartmentId { get; set; }
        public long? ResPlacingId { get; set; }
        public DateTime? SetupTime { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        public DateTime? BreakDownTime { get; set; }
        public decimal? Quantity { get; set; }
        public bool? FixedQuantity { get; set; }
        public decimal? CostPrice { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceExcl { get; set; }
        public decimal? GrossAmount { get; set; }
        public decimal? GrossAmountExcl { get; set; }
        public double? DiscountPerc { get; set; }
        public decimal? NetAmount { get; set; }
        public decimal? NetAmountExcl { get; set; }
        public string Remarks { get; set; }
        public string DescriptionMemo { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public string DescrMemoInt { get; set; }
        public bool? OnResQuote { get; set; }
        public long? PartId2 { get; set; }
        public bool? NotPrintOnReceipt { get; set; }
        public bool? AmountToArrangement { get; set; }
    }
}
