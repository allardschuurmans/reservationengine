﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Contracts
{
    public class ArrangementDto
    {
        public long ResArrangementId { get; set; }
        public string Description { get; set; }
        public string TicketText { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public string DescriptionMemo { get; set; }
        public string DescrMemoInt { get; set; }
        public string PictureFileName { get; set; }
        public bool? InternetAvail { get; set; }
        public int? OrderNumber { get; set; }
        public string Abbreviation { get; set; }
        public string DescriptionInternet { get; set; }
        
        public long? ArticleId { get; set; }
        public bool? UseAsTemplate { get; set; }
        public string DescriptionInternetMemo { get; set; }
        
        public int? MaxQuantity { get; set; }
        public int? MinQuantity { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        
        public bool? IsScheduledOnInternet { get; set; }
        public bool? IsScheduledOnBrp { get; set; }
        public bool? IsScheduledOnCashregister { get; set; }
        
        public int? MinimalQuantityAgeCategory1 { get; set; }
        public int? MinimalQuantityAgeCategory2 { get; set; }
        public int? MinimalQuantityAgeCategory3 { get; set; }
        public int? MinimalQuantityAgeCategory4 { get; set; }
    }
}
