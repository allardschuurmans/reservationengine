﻿using Application.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Poco;

namespace Application.Interfaces
{
    public interface IReservationService
    {
        Task<ReservationDto> GetReservationById(long id);

        Task<List<ReservationPartDto>> GetReservationPartById(long id, string partType, DateTime from, DateTime to);

        Task<ReservationEventDto> GetReservationEventById(long id);

        Task<List<ReservationPartDto>> GetReservationPartsByReservationId(long reservationId);

        Task<List<ArrangementDto>> GetArrangementsByReservationId(long reservationId);

        Task<List<ReservationEventDto>> GetReservationEventsByReservationId(long reservationId);

        Task<List<ResLocationOccupationDto>> GetResLocationOccupationByLocationId(long locationId, DateTime startDateTime, DateTime endDateTime);
    }
}
