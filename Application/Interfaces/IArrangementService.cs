﻿using Application.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IArrangementService
    {
        Task<ArrangementDto> GetArrangementById(long arrangementId);

        Task<ArrangementPartDto> GetArrangementPartById(long arrangementPartId);

        Task<List<ArrangementPartDto>> GetArrangementPartsById(long arrangementId, string partType);

        Task<List<ResArrPreferedScheduleDto>> GetResArrPreferedSchedule(long arrangementId);

        Task<List<ResArrPreferedScheduleDto>> GetResArrPreferedScheduleDate(long arrangementId, DateTime from,
            DateTime to);

        Task<List<ArrangementDto>> GetArrangementsByReservationId(long reservationId);
    }
}
