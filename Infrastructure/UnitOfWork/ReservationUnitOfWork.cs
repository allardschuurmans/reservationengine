using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Infrastructure.Interfaces;
using Infrastructure.Poco;
using Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.UnitOfWork
{
    public class ReservationUnitOfWork : IUnitOfWork
    {
        private readonly IDatabaseContext _context;
        
        public IGenericRepository<KeyTable> KeyTableRepository { get; set; }
        public IGenericRepository<ResArrangement> ResArrangementRepository { get; set; }
        public IGenericRepository<ResArrPart> ResPartRepository { get; set; }
        public IGenericRepository<Reservation> ReservationRepository { get; set; }
        public IGenericRepository<ResEvent> ResEventRepository { get; set; }
        public IGenericRepository<ResPart> ReservationPartRepository { get; set; }
        public IGenericRepository<ResArrPreferedSchedule> ResArrPreferedScheduleRepository { get; set; }
        public IGenericRepository<ResLocationOccupation> ResLocationOccupationRepo { get; set; }

        public ReservationUnitOfWork(IDatabaseContext context)
        {
            _context = context;

            KeyTableRepository = new GenericRepository<KeyTable>(_context);
            ResArrangementRepository = new GenericRepository<ResArrangement>(_context);
            ResPartRepository = new GenericRepository<ResArrPart>(_context);
            ReservationRepository = new GenericRepository<Reservation>(_context);
            ResEventRepository = new GenericRepository<ResEvent>(_context);
            ResArrPreferedScheduleRepository = new GenericRepository<ResArrPreferedSchedule>(_context);
            ResLocationOccupationRepo = new GenericRepository<ResLocationOccupation>(_context);
            ReservationPartRepository = new GenericRepository<ResPart>(_context);
        }



        public void Save()
        {
            _context.SaveChanges();
        }
    }
}