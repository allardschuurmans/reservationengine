﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infrastructure.Poco
{
    public class KeyTablePoco
    {
        [Key]
        public long KeyTableId { get; set; }
        public string TableName { get; set; }
        public long LastTableId { get; set; }
        public long ReplicationSite { get; set; }
        public long? MinTableId { get; set; }
        public long? MaxTableId { get; set; }
    }
}
