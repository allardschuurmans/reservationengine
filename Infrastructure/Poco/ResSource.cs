﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResSource
    {
        public ResSource()
        {
           
        }

        public long ResSourceId { get; set; }
        public string Description { get; set; }
        public string Abbreviation { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public bool? DefaultInternet { get; set; }
        public bool? DefaultCashregister { get; set; }

        
    }
}
