﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResTemplate
    {
        public long ResTemplateId { get; set; }
        public string Description { get; set; }
        public int? TemplateType { get; set; }
        public byte[] TemplateDoc { get; set; }
        public string DisplayFields { get; set; }
        public long? LanguageId { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

       
    }
}
