﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResPartActivity
    {
        public long ResPartActivityId { get; set; }
        public long? ResPartId { get; set; }
        public long? ResSubActivityId { get; set; }
        public int? Quantity { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResPart ResPart { get; set; }
        public ResSubActivity ResSubActivity { get; set; }
    }
}
