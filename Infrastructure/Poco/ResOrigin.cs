﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResOrigin
    {
        public ResOrigin()
        {
            
        }

        public long ResOriginId { get; set; }
        public string Description { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public bool? DefaultInternet { get; set; }
        public bool? DefaultBackoffice { get; set; }
        public bool? DefaultCashregister { get; set; }

       
    }
}
