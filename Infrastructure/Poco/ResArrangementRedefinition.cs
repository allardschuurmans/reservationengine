﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResArrangementRedefinition
    {
        public long ResArrangementRedefinitionId { get; set; }
        public long? ConfigurationId { get; set; }
        public long? ResArrangementId { get; set; }
        public string Description { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

     
    }
}
