﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResActLocation
    {
        public ResActLocation()
        {
           
        }

        public long ResActLocationId { get; set; }
        public string Description { get; set; }
        public string DescriptionMemo { get; set; }
        public string DescrMemoInt { get; set; }
        public int? OrderNumber { get; set; }
        public int? TotalUnits { get; set; }
        public DateTime? MinResTime { get; set; }
        public int? DisplayColor { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

       
    }
}
