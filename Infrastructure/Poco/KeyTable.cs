﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class KeyTable
    {
        public long KeyTableId { get; set; }
        public string TableName { get; set; }
        public long? LastTableId { get; set; }
        public long? ReplicationSite { get; set; }
        public long? MinTableId { get; set; }
        public long? MaxTableId { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
    }
}
