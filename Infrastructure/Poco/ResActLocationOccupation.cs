﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResActLocationOccupation
    {
        public long ResActLocationOccupationId { get; set; }
        public long? ResPartId { get; set; }
        public long? ResActLocationId { get; set; }
        public long? ResActivityId { get; set; }
        public long? ResSubActivityId { get; set; }
        public long? ResActLocationBlockId { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        public int? Units { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResActLocation ResActLocation { get; set; }
        public ResActLocationBlock ResActLocationBlock { get; set; }
        public ResActivity ResActivity { get; set; }
        public ResPart ResPart { get; set; }
        public ResSubActivity ResSubActivity { get; set; }
    }
}
