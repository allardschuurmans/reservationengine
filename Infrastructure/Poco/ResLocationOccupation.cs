﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResLocationOccupation
    {
        public long ResLocationOccupationId { get; set; }
        public long? ResPartId { get; set; }
        public long? ResLocationId { get; set; }
        public long? ResLocationBlockId { get; set; }
        public long? ResOptionDateId { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        public DateTime? SetupTime { get; set; }
        public DateTime? BreakDownTime { get; set; }
        public DateTime? StartsWithSetup { get; set; }
        public DateTime? EndsWithBreakDown { get; set; }
        public int? Quantity { get; set; }
        public int? OccupationQuantity { get; set; }
        public bool? DueToOtherOccupation { get; set; }
        public long? OrgResLocationId { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public bool? Exclusive { get; set; }
        public long? RmkindDetailId { get; set; }

        public ResLocation ResLocation { get; set; }
        public ResLocationBlock ResLocationBlock { get; set; }
        public ResOptionDate ResOptionDate { get; set; }
        public ResPart ResPart { get; set; }
        
    }
}
