﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResActivityPlacing
    {
        public long ResActivityPlacingId { get; set; }
        public long? ResActLocationPlacingId { get; set; }
        public long? ResActivityId { get; set; }
        public int? TotalUnits { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResActLocationPlacing ResActLocationPlacing { get; set; }
        public ResActivity ResActivity { get; set; }
    }
}
