﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class RespartDepartment
    {
        public long RespartDepartmentId { get; set; }
        public long? ResPartId { get; set; }
        public long? DepartmentId { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
    }
}
