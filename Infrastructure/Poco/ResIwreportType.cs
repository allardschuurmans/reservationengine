﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResIwreportType
    {
        public ResIwreportType()
        {
           
        }

        public long ResIwreportTypeId { get; set; }
        public string Description { get; set; }
        public int? OrderNumber { get; set; }
        public bool? IntranetAvail { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        
    }
}
