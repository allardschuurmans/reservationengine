﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResQuote
    {
        public ResQuote()
        {
          
        }

        public long ResQuoteId { get; set; }
        public long? ResEventId { get; set; }
        public string Description { get; set; }
        public int? QuoteType { get; set; }
        public byte[] QuoteDoc { get; set; }
        public string DisplayFields { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string EmailAttachment { get; set; }
        public DateTime? RemovedDate { get; set; }
        public long? RemovedUser { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public long? GenericOrderId { get; set; }
        public long? ResTemplateRbid { get; set; }
        public long? SourceResQuoteId { get; set; }

        
        public ResEvent ResEvent { get; set; }
        public ResTemplateRb ResTemplateRb { get; set; }
        public ResQuote SourceResQuote { get; set; }
       
    }
}
