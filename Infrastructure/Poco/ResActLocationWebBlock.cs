﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResActLocationWebBlock
    {
        public long ResActLocationWebBlockId { get; set; }
        public long? ResActLocationId { get; set; }
        public DateTime? FromDateTime { get; set; }
        public bool? Blocked { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResActLocation ResActLocation { get; set; }
    }
}
