﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResActLocationSchedule
    {
        public long ResActLocationScheduleId { get; set; }
        public long? ResActLocationId { get; set; }
        public long? ResActPlacingId { get; set; }
        public DateTime? FromDateTime { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResActLocation ResActLocation { get; set; }
        public ResActPlacing ResActPlacing { get; set; }
    }
}
