﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResConstrainingActivity
    {
        public ResConstrainingActivity()
        {
            
        }

        public long ResConstrainingActivityId { get; set; }
        public long? ResActivityId { get; set; }
        public long? ResActLocationPlacing { get; set; }
        public int? Lowerbound { get; set; }
        public int? Upperbound { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResActLocationPlacing ResActLocationPlacingNavigation { get; set; }
        public ResActivity ResActivity { get; set; }
        
    }
}
