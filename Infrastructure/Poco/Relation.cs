﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class Relation
    {
        public Relation()
        {
        
        }

        public long RelationId { get; set; }
        public long? RelationGroupingId { get; set; }
        public bool? Blocked { get; set; }
        public DateTime? BlockedFromDate { get; set; }
        public string SearchName { get; set; }
        public string BadgeNumber { get; set; }
        public string ExpRelationId { get; set; }
        public int AdressKind { get; set; }
        public string CompanyName { get; set; }
        public string CompanyDepartment { get; set; }
        public string Salutation { get; set; }
        public string Title { get; set; }
        public string Initials { get; set; }
        public string SurName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SubName { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string HouseNumber { get; set; }
        public string Zipcode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string FaxNo { get; set; }
        public string PrivatePhoneNo { get; set; }
        public string MobilePhoneNo { get; set; }
        public long? TerritoryId { get; set; }
        public long? CountryId { get; set; }
        public long? LanguageId { get; set; }
        public string Remarks { get; set; }
        public DateTime? Birthday { get; set; }
        public int? BirthdayDay { get; set; }
        public int? BirthdayMonth { get; set; }
        public DateTime? CreationDate { get; set; }
        public DateTime? EndContactDate { get; set; }
        public string BankNumber { get; set; }
        public string PostalBankNumber { get; set; }
        public string Vatnumber { get; set; }
        public string Gender { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyKind { get; set; }
        public string HomePage { get; set; }
        public long? ModifyUser { get; set; }
        public long? LinkedRelationId { get; set; }
        public long? ProspectId { get; set; }
        public long? RelationSalutationId { get; set; }
        public string Password { get; set; }
        public long? PaymentConditionId { get; set; }
        public bool? NewsLetter { get; set; }
        public long? SegmentId { get; set; }
        public string RegistrationNumber { get; set; }
        public string OptionT1 { get; set; }
        public string OptionT2 { get; set; }
        public string OptionT3 { get; set; }
        public string OptionT4 { get; set; }
        public string OptionT5 { get; set; }
        public bool? OptionB1 { get; set; }
        public bool? OptionB2 { get; set; }
        public bool? OptionB3 { get; set; }
        public bool? OptionB4 { get; set; }
        public bool? OptionB5 { get; set; }
        public DateTime? OptionD1 { get; set; }
        public DateTime? OptionD2 { get; set; }
        public bool? DirectDebit { get; set; }
        public string DirectDebitName { get; set; }
        public string DirectDebitCity { get; set; }
        public bool? Invoice { get; set; }
        public bool? DirectPay { get; set; }
        public bool? IsSupplier { get; set; }
        public string Ngfnumber { get; set; }
        public long? MemberKindId { get; set; }
        public DateTime? MemberFromDate { get; set; }
        public DateTime? MemberToDate { get; set; }
        public string CorrespondAdress { get; set; }
        public string CorrespondHouseNumber { get; set; }
        public string CorrespondZipCode { get; set; }
        public string CorrespondCity { get; set; }
        public long? BillingRelationId { get; set; }
        public bool? Gvb { get; set; }
        public DateTime? GvbDate { get; set; }
        public decimal? EgaHandicap { get; set; }
        public string NgfMemberDescription { get; set; }
        public string Kvknumber { get; set; }
        public bool? CheckArticleRedefinition { get; set; }
        public string Alert { get; set; }
        public long? DebtorRelationId { get; set; }
        public int? DuplicateCount { get; set; }
        public bool? IsSmartInput { get; set; }
        public string ExpRelationId2 { get; set; }
        public long? BalanceRelationId { get; set; }
        public string DeleveryAdress { get; set; }
        public string DeleveryHouseNumber { get; set; }
        public string DeleveryZipCode { get; set; }
        public string DeleveryCity { get; set; }
        public long? DeleveryCountryId { get; set; }
        public string OptionT6 { get; set; }
        public string OptionT7 { get; set; }
        public string OptionT8 { get; set; }
        public string OptionT9 { get; set; }
        public string OptionT10 { get; set; }
        public string BlockedReason { get; set; }
        public long? RelationBlockReasonId { get; set; }
        public bool? IsNewMember { get; set; }
        public bool? MembershipIsEnded { get; set; }
        public DateTime? MembershipEndedDate { get; set; }
        public bool? Exported { get; set; }
        public string InvoiceAdress { get; set; }
        public string InvoiceHouseNumber { get; set; }
        public string InvoiceZipCode { get; set; }
        public string InvoiceCity { get; set; }
        public long? InvoiceCountryId { get; set; }
        public long? RepresentativeId { get; set; }
        public bool? IsSubventionRelation { get; set; }
        public string ExternalReferenceId { get; set; }
        public long? ReportDxid { get; set; }
        public string Status { get; set; }
        public int? RelationInformationPrivacyKind { get; set; }
        public long? DirectDebitRelationId { get; set; }
        public string LicensePlateRegistration { get; set; }
        public string LicensePlateCountryCode { get; set; }
        public long? PlanPlanInternetId { get; set; }
        public string Iban { get; set; }
        public string Bic { get; set; }
        public DateTime? DirectDebitSignatureDate { get; set; }
        public long? BirthCountryId { get; set; }
        public string ActivationCode { get; set; }
        public string JobTitle { get; set; }
        public long? NativeLanguageId { get; set; }
        public long? CorrespondCountryId { get; set; }
        public long? InvoiceMailLayoutId { get; set; }
        public long? InvoiceHistoricMailLayoutId { get; set; }
        public DateTime? EmailForgottenDateTime { get; set; }
        public bool? OverrideRelationGroupInvoiceMailLayout { get; set; }
        public bool? OverrideRelationGroupInvoiceHistoricMailLayout { get; set; }
        public long? RelationStatusId { get; set; }
        public bool? Verified { get; set; }

        public Relation BalanceRelation { get; set; }
        public Relation BillingRelation { get; set; }
        
        public Relation DebtorRelation { get; set; }
      
     
    }
}
