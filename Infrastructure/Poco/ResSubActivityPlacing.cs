﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResSubActivityPlacing
    {
        public long ResSubActivityPlacingId { get; set; }
        public long? ResActivityPlacingId { get; set; }
        public long? ResSubActivityId { get; set; }
        public int? TotalUnits { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
    }
}
