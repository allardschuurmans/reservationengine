﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResArrSuperSel
    {
        public long ResArrSuperSelId { get; set; }
        public long? ResArrangementId { get; set; }
        public long? ResArrSuperId { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

     
    }
}
