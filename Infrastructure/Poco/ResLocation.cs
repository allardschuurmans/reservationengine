﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResLocation
    {
        public ResLocation()
        {
            
        }

        public long ResLocationId { get; set; }
        public string Description { get; set; }
        public string DescriptionMemo { get; set; }
        public int? OrderNumber { get; set; }
        public bool? BySeat { get; set; }
        public int? TotalSeats { get; set; }
        public int? Dimension { get; set; }
        public DateTime? MinResTime { get; set; }
        public DateTime? SetupTime { get; set; }
        public DateTime? BreakdownTime { get; set; }
        public long? ParentResLocationId { get; set; }
        public long? DepartmentId { get; set; }
        public long? ArticleId { get; set; }
        public int? DisplayColor { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public string DescrMemoInt { get; set; }
        public bool? OnResQuote { get; set; }
        public string Abbreviation { get; set; }
        public long? AlternativeResLocationId { get; set; }
        public long? RmkindId { get; set; }
        public long? PersonelWorkingLocationId { get; set; }

     
      
        public ResLocation ParentResLocation { get; set; }
       
       
    }
}
