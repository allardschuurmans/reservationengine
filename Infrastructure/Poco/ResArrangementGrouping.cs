﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResArrangementGrouping
    {
        public long ResArrangementGroupingId { get; set; }
        public long? ResArrangementId { get; set; }
        public long? ResGroupingId { get; set; }
        public string Modifykind { get; set; }
        public long? ModifyUser { get; set; }
        public DateTime? ModifyDate { get; set; }

       
    }
}
