﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResTicket
    {
        public long ResTicketId { get; set; }
        public string Description { get; set; }
        public string DescriptionMemo { get; set; }
        public long? ArticleId { get; set; }
        public long? DepartmentId { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public string DescrMemoInt { get; set; }
        public bool? OnResQuote { get; set; }

     
    }
}
