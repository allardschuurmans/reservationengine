﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResStatus
    {
        public ResStatus()
        {
            
        }

        public long ResStatusId { get; set; }
        public string Description { get; set; }
        public string Abbreviation { get; set; }
        public int? OrderNumber { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public bool? DefaultInternet { get; set; }
        public bool? DefaultCashregister { get; set; }
        public bool? MailingCustomerPayOnInternet { get; set; }
        public bool? PaymentDone { get; set; }

        
    }
}
