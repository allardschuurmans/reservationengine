﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResSpecialDay
    {
        public long ResSpecialDayId { get; set; }
        public string Description { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
    }
}
