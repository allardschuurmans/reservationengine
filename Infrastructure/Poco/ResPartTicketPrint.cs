﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResPartTicketPrint
    {
        public ResPartTicketPrint()
        {
           
        }

        public long ResPartTicketPrintId { get; set; }
        public long? ResPartId { get; set; }
        public int? TicketSequenceNr { get; set; }
        public int? Entrances { get; set; }
        public long? ArticleId { get; set; }
        public string Barcode { get; set; }
        public DateTime? TicketDate { get; set; }
        public DateTime? PrintDate { get; set; }
        public long? PrintUser { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        
        public ResPart ResPart { get; set; }
       
    }
}
