﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResArrPreferedScheduleSource
    {
        public ResArrPreferedScheduleSource()
        {
            
        }

        public long ResArrPreferedScheduleSourceId { get; set; }
        public string Description { get; set; }
        public bool? DefaultBrp { get; set; }
        public bool? DefaultCashregister { get; set; }
        public bool? DefaultInternet { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        
    }
}
