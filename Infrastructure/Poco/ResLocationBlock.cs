﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResLocationBlock
    {
        public ResLocationBlock()
        {
            
        }

        public long ResLocationBlockId { get; set; }
        public long? ResLocationId { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
        public bool? BlockComplete { get; set; }
        public int? BlockSeats { get; set; }
        public string Reason { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResLocation ResLocation { get; set; }
        
    }
}
