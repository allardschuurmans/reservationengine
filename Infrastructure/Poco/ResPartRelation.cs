﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResPartRelation
    {
        public long ResPartRelationId { get; set; }
        public long? ResPartId { get; set; }
        public long? ResEventRelationId { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResEventRelation ResEventRelation { get; set; }
        public ResPart ResPart { get; set; }
    }
}
