﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResArrangementCourse
    {
        public long ResArrangementCourseId { get; set; }
        public long? ResArrangementId { get; set; }
        public string Description { get; set; }
        public long? CourseLanguageId { get; set; }
        public long? MotherTongueLanguageId { get; set; }
        public long? ResCourceStartLevelId { get; set; }
        public long? ResCourceEndLevelId { get; set; }
        public string PriceDescription { get; set; }
        public bool? Certificate { get; set; }
        public int? NumberOfCredits { get; set; }
        public string DescriptionMemo1 { get; set; }
        public string DescriptionMemo2 { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
    }
}
