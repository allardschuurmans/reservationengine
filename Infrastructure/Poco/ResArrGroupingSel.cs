﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResArrGroupingSel
    {
        public long ResArrGroupingSelId { get; set; }
        public long? ResArrangementId { get; set; }
        public long? ResArrGroupingId { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

      
    }
}
