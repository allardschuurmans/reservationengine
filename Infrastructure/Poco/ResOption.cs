﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResOption
    {
        public ResOption()
        {
           
        }

        public long ResOptionId { get; set; }
        public long? ResEventId { get; set; }
        public string Description { get; set; }
        public DateTime? CreationDate { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResEvent ResEvent { get; set; }
        
    }
}
