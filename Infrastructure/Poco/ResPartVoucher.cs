﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResPartVoucher
    {
        public long ResPartVoucherId { get; set; }
        public long? ResPartId { get; set; }
        public long? VoucherId { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResPart ResPart { get; set; }
        
    }
}
