﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResInternalMemo
    {
        public long ResInternalMemoId { get; set; }
        public long ReservationId { get; set; }
        public string Memo { get; set; }
        public string ModifyKind { get; set; }
        public long? ModifyUser { get; set; }
        public DateTime? ModifyDate { get; set; }

        public Reservation Reservation { get; set; }
    }
}
