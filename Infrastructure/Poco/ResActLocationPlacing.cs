﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResActLocationPlacing
    {
        public ResActLocationPlacing()
        {
        
        }

        public long ResActLocationPlacingId { get; set; }
        public long? ResActLocationId { get; set; }
        public long? ResActPlacingId { get; set; }
        public int? TotalUnits { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResActLocation ResActLocation { get; set; }
        public ResActPlacing ResActPlacing { get; set; }
       
    }
}
