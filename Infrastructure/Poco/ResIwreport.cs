﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResIwreport
    {
        public long ResIwreportId { get; set; }
        public string Description { get; set; }
        public long? ResIwreportTypeId { get; set; }
        public string IwreportQueries { get; set; }
        public string IwreportTemplate { get; set; }
        public string IwreportParams { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResIwreportType ResIwreportType { get; set; }
    }
}
