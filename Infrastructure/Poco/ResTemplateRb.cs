﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResTemplateRb
    {
        public ResTemplateRb()
        {
          
        }

        public long ResTemplateRbid { get; set; }
        public string Description { get; set; }
        public int? TemplateType { get; set; }
        public string TemplateSql { get; set; }
        public string TemplateRpt { get; set; }
        public long? LanguageId { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string EmailAttachment { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        
    }
}
