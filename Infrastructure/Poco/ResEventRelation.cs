﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResEventRelation
    {
        public ResEventRelation()
        {
            
        }

        public long ResEventRelationId { get; set; }
        public long? ResEventId { get; set; }
        public long? RelationId { get; set; }
        public string Description { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public string TicketBarcode { get; set; }
        public bool? TicketPrinted { get; set; }
        public DateTime? TicketPrintDate { get; set; }
        public string WetsuitId { get; set; }
        public string BeltId { get; set; }
        public bool? VideoPayed { get; set; }
        public string VideoDownloadUrl { get; set; }
        public DateTime? VideoDownloadDate { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string WetsuitSize { get; set; }
        public bool? AcceptTerms { get; set; }
        public bool? InChecked { get; set; }

        public Relation Relation { get; set; }
        public ResEvent ResEvent { get; set; }
        
    }
}
