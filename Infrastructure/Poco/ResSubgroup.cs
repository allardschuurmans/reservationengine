﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResSubgroup
    {
        public long ResSubgroupId { get; set; }
        public string Description { get; set; }
        public long? ModifyUser { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyKind { get; set; }
    }
}
