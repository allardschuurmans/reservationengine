﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResQuoteLog
    {
        public long ResQuoteLogId { get; set; }
        public long? ResQuoteId { get; set; }
        public int? LogAction { get; set; }
        public string LogActionTo { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResQuote ResQuote { get; set; }
    }
}
