﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResReport
    {
        public long ResReportId { get; set; }
        public string Description { get; set; }
        public long? ResReportTypeId { get; set; }
        public string TableName { get; set; }
        public string ReportQueries { get; set; }
        public string ReportTemplate { get; set; }
        public string ReportParams { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public string EmailAttachment { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResReportType ResReportType { get; set; }
    }
}
