﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResPartTicket
    {
        public long ResPartTicketId { get; set; }
        public long? TicketPartId { get; set; }
        public long? PrintPartId { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResPart PrintPart { get; set; }
        public ResPart TicketPart { get; set; }
    }
}
