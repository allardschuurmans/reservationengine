﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResTodo
    {
        public long ResTodoId { get; set; }
        public long? ResEventId { get; set; }
        public long? ReservationId { get; set; }
        public long? ResPartId { get; set; }
        public string Keywords { get; set; }
        public string DescriptionMemo { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime? CreationDate { get; set; }
        public long? CreationUser { get; set; }
        public DateTime? ExecutionDate { get; set; }
        public long? ExecutionUser { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResEvent ResEvent { get; set; }
        public ResPart ResPart { get; set; }
        public Reservation Reservation { get; set; }
    }
}
