﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResCancelCause
    {
        public ResCancelCause()
        {
           
        }

        public long ResCancelCauseId { get; set; }
        public string Description { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

      
    }
}
