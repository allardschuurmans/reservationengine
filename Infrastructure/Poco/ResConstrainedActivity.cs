﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResConstrainedActivity
    {
        public long ResConstrainedActivityId { get; set; }
        public long? ResConstrainingActivityId { get; set; }
        public long? ResActivityId { get; set; }
        public int? MaxUnits { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? Modifyuser { get; set; }

        public ResActivity ResActivity { get; set; }
        public ResConstrainingActivity ResConstrainingActivity { get; set; }
    }
}
