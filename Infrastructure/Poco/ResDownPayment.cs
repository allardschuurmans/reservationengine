﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResDownPayment
    {
        public long ResDownPaymentId { get; set; }
        public long? ResEventId { get; set; }
        public DateTime? PaymentDate { get; set; }
        public decimal? Amount { get; set; }
        public string Remarks { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public long? VoucherId { get; set; }
        public short? ExportNr { get; set; }
        public bool? Processed { get; set; }
        public decimal? Amount2 { get; set; }
        public long? PaymentId { get; set; }
        public long? JournalHeaderId { get; set; }
        public string ExternalReference { get; set; }

     
        public ResEvent ResEvent { get; set; }
      
    }
}
