﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResReportType
    {
        public ResReportType()
        {
            
        }

        public long ResReportTypeId { get; set; }
        public string Description { get; set; }
        public int? OrderNumber { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

       
    }
}
