﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResLog
    {
        public long ResLogId { get; set; }
        public long? ResEventId { get; set; }
        public long? ReservationId { get; set; }
        public long? ResPartId { get; set; }
        public long? TicketSequenceNr { get; set; }
        public string RemarksMemo { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public string Action { get; set; }
        public long? UsersId { get; set; }

        public ResEvent ResEvent { get; set; }
        public ResPart ResPart { get; set; }
        public Reservation Reservation { get; set; }
    }
}
