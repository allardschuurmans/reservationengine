﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResSubActivity
    {
        public ResSubActivity()
        {
           
        }

        public long ResSubActivityId { get; set; }
        public string Description { get; set; }
        public string DescriptionMemo { get; set; }
        public string DescrMemoInt { get; set; }
        public int? OrderNumber { get; set; }
        public int? UnitSize { get; set; }
        public int? TotalSeats { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

    
    }
}
