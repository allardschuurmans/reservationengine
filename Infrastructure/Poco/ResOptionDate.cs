﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResOptionDate
    {
        public ResOptionDate()
        {
           
        }

        public long ResOptionDateId { get; set; }
        public long? ResOptionId { get; set; }
        public long? ReservationId { get; set; }
        public DateTime? OptionDate { get; set; }
        public DateTime? DeltaTime { get; set; }
        public bool? DeltaNegative { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResOption ResOption { get; set; }
        public Reservation Reservation { get; set; }
       
    }
}
