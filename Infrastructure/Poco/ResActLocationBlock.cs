﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResActLocationBlock
    {
        public ResActLocationBlock()
        {
          
        }

        public long ResActLocationBlockId { get; set; }
        public long? ResActLocationId { get; set; }
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
        public bool? BlockComplete { get; set; }
        public int? BlockUnits { get; set; }
        public string Reason { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResActLocation ResActLocation { get; set; }
       
    }
}
