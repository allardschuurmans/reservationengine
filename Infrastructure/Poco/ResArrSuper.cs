﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResArrSuper
    {
        public ResArrSuper()
        {
           
        }

        public long ResArrSuperId { get; set; }
        public string Description { get; set; }
        public string DescriptionMemo { get; set; }
        public string DescrMemoInt { get; set; }
        public int? OrderNumber { get; set; }
        public string PictureFilename { get; set; }
        public bool? InternetAvail { get; set; }
        public int? SelectQuantity { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public string Abbreviation { get; set; }

        
    }
}
