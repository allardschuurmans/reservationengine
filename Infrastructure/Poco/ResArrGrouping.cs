﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResArrGrouping
    {
        public ResArrGrouping()
        {
            
        }

        public long ResArrGroupingId { get; set; }
        public string Description { get; set; }
        public string DescriptionMemo { get; set; }
        public string DescrMemoInt { get; set; }
        public int? OrderNumber { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
        public string PictureFilename { get; set; }
        public bool? InternetAvail { get; set; }
        public string DescriptionInternet { get; set; }
        public string DescriptionInternetMemo { get; set; }
        public byte[] Picture { get; set; }

        
    }
}
