﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResLocationPlacing
    {
        public long ResLocationPlacingId { get; set; }
        public long? ResLocationId { get; set; }
        public long? ResPlacingId { get; set; }
        public int? TotalSeats { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }

        public ResLocation ResLocation { get; set; }
        public ResPlacing ResPlacing { get; set; }
    }
}
