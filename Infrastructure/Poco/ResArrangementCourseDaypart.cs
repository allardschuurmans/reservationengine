﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResArrangementCourseDaypart
    {
        public long ResArrangementCourseDaypartId { get; set; }
        public long? ResArrangementCourseId { get; set; }
        public string Description { get; set; }
        public long? TimeTableSetId { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public string ModifyKind { get; set; }
        public DateTime? ModifyDate { get; set; }
        public long? ModifyUser { get; set; }
    }
}
