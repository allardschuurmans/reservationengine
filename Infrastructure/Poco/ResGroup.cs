﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Poco
{
    public partial class ResGroup
    {
        public ResGroup()
        {
           
        }

        public long ResGroupId { get; set; }
        public string Description { get; set; }
        public long? ModifyUser { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyKind { get; set; }
        public bool? DefaultInternet { get; set; }
        public int? DisplayColor { get; set; }

        
    }
}
