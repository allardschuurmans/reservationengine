using System;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Poco;
using Infrastructure.Interfaces;
using System.Threading.Tasks;

namespace Infrastructure.Context
{
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        public DatabaseContext()
        {

        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
        : base(options)
        {
        }

        public DbSet<T> GetDbSet<T>() where T : class
        {
            return Set<T>();
        }

        public System.Threading.Tasks.Task SaveChangesAsync()
        {
            return SaveChangesAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<KeyTable>().ToTable("KeyTable");
            modelBuilder.Entity<ResArrangement>().ToTable("ResArrangement");
            modelBuilder.Entity<ResArrPart>().ToTable("ResArrPart");
            modelBuilder.Entity<Reservation>().ToTable("Reservation");
            modelBuilder.Entity<ResEvent>().ToTable("ResEvent");
            modelBuilder.Entity<ResArrPreferedSchedule>().ToTable("ResArrPreferedSchedule");
            modelBuilder.Entity<ResLocationOccupation>().ToTable("ResLocationOccupation");

        }

        

        void IDatabaseContext.SaveChanges()
        {
            SaveChanges();
        }
       
    }

}