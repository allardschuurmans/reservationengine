using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Poco;

namespace Infrastructure.Interfaces
{
    public interface ITableIdProvider<T> where T : class
    {
        long GetTableId(T entity);
    }

}