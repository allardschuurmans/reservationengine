using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Interfaces
{
    public interface IDatabaseContext
    {
        DbSet<T> GetDbSet<T>() where T : class;
        void SaveChanges();
        void Dispose();
    }
}