using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Infrastructure.Poco;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Interfaces
{
    public interface IUnitOfWork
    {
        IGenericRepository<KeyTable> KeyTableRepository { get; set; }
        IGenericRepository<ResArrangement> ResArrangementRepository { get; set; }
        IGenericRepository<ResArrPart> ResPartRepository { get; set; }
        IGenericRepository<Reservation> ReservationRepository { get; set; }
        IGenericRepository<ResEvent> ResEventRepository { get; set; }
        IGenericRepository<ResPart> ReservationPartRepository { get; set; }
        IGenericRepository<ResArrPreferedSchedule> ResArrPreferedScheduleRepository { get; set; }
        IGenericRepository<ResLocationOccupation> ResLocationOccupationRepo { get; set; }

        void Save();
    }
}