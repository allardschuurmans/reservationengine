using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Interfaces;
using Infrastructure.Poco;

namespace Infrastructure.Repositories
{

    public class GenericRepository<T> : IGenericRepository<T>, IDisposable where T : class
    {
        private readonly IDatabaseContext _context;

        public GenericRepository(IDatabaseContext context)
        {
            _context = context;
        }

        public T Add(T entity)
        {
            if (typeof(T) == typeof(ResArrangement))
            {
                HandleKeyTableResArrangement(entity);
            }
            else if (typeof(T) == typeof(ResArrPart))
            {
                HandleKeyTableResPart(entity);
            }
            else if (typeof(T) == typeof(ResEvent))
            {
                HandleKeyTableResEvent(entity);
            }
            else if (typeof(T) == typeof(Reservation))
            {
                HandleKeyTableReservation(entity);
            }

            var obj = _context.GetDbSet<T>().Add(entity);

            return obj.Entity;
        }

        public void Delete(T entity)
        {
            throw new NotImplementedException();
        }
        public void Edit(T entity)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public T GetEntity(long id)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }
        
        public Task<IQueryable<T>> GetAllAsync()
        {
            return Task.Run(() => _context.GetDbSet<T>().AsQueryable());
        }

        public async Task<T> GetEntityAsync(long id)
        {
            return await _context.GetDbSet<T>().FindAsync(id);
        }

        public async Task<IQueryable<T>> FindByAsync(Expression<Func<T, bool>> predicate)
        {
            return await Task.Run( ()=> _context.GetDbSet<T>().Where(predicate));
        }


        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        #region KeyTableHandlers

        private void HandleKeyTableResArrangement(T entity)
        {
            var latestKey = _context.GetDbSet<KeyTable>().LastOrDefault(m => m.TableName == "ResArrangement");
            var resArrangement = (ResArrangement)Convert.ChangeType(entity, typeof(ResArrangement));
            latestKey.LastTableId = latestKey.LastTableId + 1;
            resArrangement.ResArrangementId = latestKey.LastTableId.Value;

            _context.GetDbSet<KeyTable>().Update(latestKey);
        }

        private void HandleKeyTableResPart(T entity)
        {
            var latestKey = _context.GetDbSet<KeyTable>().LastOrDefault(m => m.TableName == "ResArrPart");
            var resArrangement = (ResArrPart)Convert.ChangeType(entity, typeof(ResArrPart));
            latestKey.LastTableId = latestKey.LastTableId + 1;
            resArrangement.ResArrPartId = latestKey.LastTableId.Value;

            _context.GetDbSet<KeyTable>().Update(latestKey);
        }

        private void HandleKeyTableReservation(T entity)
        {
            var latestKey = _context.GetDbSet<KeyTable>().LastOrDefault(m => m.TableName == "Reservation");
            var resArrangement = (Reservation)Convert.ChangeType(entity, typeof(Reservation));
            latestKey.LastTableId = latestKey.LastTableId + 1;
            resArrangement.ReservationId = latestKey.LastTableId.Value;

            _context.GetDbSet<KeyTable>().Update(latestKey);
        }

        private void HandleKeyTableResEvent(T entity)
        {
            var latestKey = _context.GetDbSet<KeyTable>().LastOrDefault(m => m.TableName == "ResEvent");
            var resArrangement = (ResEvent)Convert.ChangeType(entity, typeof(ResEvent));
            latestKey.LastTableId = latestKey.LastTableId + 1;
            resArrangement.ResEventId = latestKey.LastTableId.Value;

            _context.GetDbSet<KeyTable>().Update(latestKey);
        }


        #endregion
    }
}