using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using Infrastructure.Poco;

namespace Infrastructure.KeyTableHandlers
{
    public class TableIdProvider<T> : ITableIdProvider<T> where T : class
    {
        private readonly IDatabaseContext _context;

        public TableIdProvider(IDatabaseContext context)
        {
            _context = context;
        }

        public long GetTableId(T entity)
        {
            var latestKey = _context.GetDbSet<KeyTable>().LastOrDefault(m => m.TableName == typeof(T).Name);

            if (latestKey.LastTableId.HasValue)
            {
                return latestKey.LastTableId.Value + 1;
            }

            throw new Exception();
        }
    }
}